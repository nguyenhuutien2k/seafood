// mở modal chi tiết sản phẩm
var details = document.getElementsByClassName("detail-eye");
for (var i = 0; i < details.length; i++) {
  details[i].addEventListener("click", function (event) {
    if (window.innerWidth > 992) {
      event.preventDefault();
      document.getElementById("opacity").style.display = "block";
      document.getElementById("detail-product-modal").classList.add("active");
    }
  });
}
//  mở modal giỏ hàng
var cartModal = document.getElementsByClassName("cart-item-modal");
for (var i = 0; i < cartModal.length; i++) {
  cartModal[i].addEventListener("click", function (event) {
    event.preventDefault();
    document.getElementById("opacity").style.display = "block";
    document.getElementById("cart-box-modal").classList.add("active");
  });
}
// click để chuyển ảnh trong modal chi tiết sản phẩm
var items = document.querySelectorAll("#list-small .image-item div");
for (var i = 0; i < items.length; i++) {
  items[i].addEventListener("click", function () {
    for (var i = 0; i < items.length; i++) {
      items[i].classList.remove("active1");
    }
    this.classList.add("active1");
    document
      .getElementById("img_01")
      .setAttribute(
        "src",
        this.getElementsByTagName("img")[0].getAttribute("data-image")
      );
    document
      .getElementById("img_01")
      .setAttribute(
        "alt",
        this.getElementsByTagName("img")[0].getAttribute("alt")
      );
  });
}
// show ảnh trong owl
$(document).ready(function () {
  var owl = $("#list-small");

  owl.owlCarousel({
    loop: false,
    rewind: true,
    margin: 5,
    nav: true,
    dots: false,
    navText: [
      ' <i class="fa fa-arrow-left" aria-hidden="true"></i>',
      '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
    ],
    responsive: {
      0: {
        items: 4,
      },
    },
  });
});
