$(document).ready(function () {
  var owl = $("#list-small");

  owl.owlCarousel({
    loop: false,
    rewind: true,
    margin: 10,
    padding: 10,
    nav: true,
    dots: false,
    navText: [
      ' <i class="fa fa-arrow-left" aria-hidden="true"></i>',
      '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
    ],
    responsive: {
      500: {
        items: 3,
      },
      990: { items: 5 },
    },
  });
});

var items = document.querySelectorAll("#list-small .item-image-small div");
function ZoomImage(obj) {
  obj.classList.add("active1");
}
for (var i = 0; i < items.length; i++) {
  items[i].addEventListener("click", function () {
    for (var i = 0; i < items.length; i++) {
      items[i].classList.remove("active1");
    }
    ZoomImage(this);
  });
}

$("#zoom_image").elevateZoom({
  gallery: "list-small",
  cursor: "pointer",
  easing: true,
  zoomWindowPosition: 1,
  zoomFadeIn: 1000,
  zoomFadeOut: 1000,
  scrollZoom: true,
  imageCrossfade: true,
  loadingIcon: "http://www.elevateweb.co.uk/spinner.gif",
});

//pass the images to Fancybox
$("#zoom_image").bind("click", function (e) {
  var ez = $("#zoom_image").data("elevateZoom");
  $.fancybox(ez.getGalleryList());
  return false;
});
// input[type="number"]

$(document).ready(function () {
  jQuery(
    '<div class="quantity-nav"><button class="quantity-button quantity-down">-</button><button class="quantity-button quantity-up">+</button></div>'
  ).insertAfter(".number-pro input");
  jQuery(".number-pro").each(function () {
    var spinner = jQuery(this),
      input = spinner.find('input[type="number"]'),
      btnUp = spinner.find(".quantity-up"),
      btnDown = spinner.find(".quantity-down"),
      min = input.attr("min"),
      max = input.attr("max");

    btnUp.click(function () {
      var oldValue = parseFloat(input.val());
      if (oldValue >= max) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue + 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

    btnDown.click(function () {
      var oldValue = parseFloat(input.val());
      if (oldValue <= min) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue - 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });
  });
});

var inputRadio = document.querySelectorAll(".select-info input[type='radio']");
for (var i = 0; i < inputRadio.length; i++) {
  inputRadio[i].addEventListener("click", function () {
    for (var i = 0; i < inputRadio.length; i++) {
      inputRadio[i].closest("label").classList.remove("active");
    }

    this.closest("label").classList.add("active");
  });
}

// js tab

var tablinks = document.querySelectorAll(".tab .tablinks");
tabcontent = document.getElementsByClassName("tabcontent");
for (var i = 0; i < tablinks.length; i++) {
  tablinks[i].addEventListener("click", function () {
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].classList.remove("active");
    }
    for (var i = 0; i < tablinks.length; i++) {
      tablinks[i].classList.remove("active");
    }
    console.log(this.getAttribute("data-link"));
    document
      .getElementById(this.getAttribute("data-link"))
      .classList.add("active");
    this.classList.add("active");
  });
}

function openCity() {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

//
