$(document).ready(function () {
  var owl1 = $("#owl-slider");

  owl1.owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 5000,
    nav: true,
    dots: true,
    navText: [
      ' <i class="fa fa-arrow-left" aria-hidden="true"></i>',
      '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
    ],
    responsive: {
      0: {
        items: 1,
      },
    },
  });

  var owl2 = $("#blog-index-list");
  owl2.owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 5000,
    nav: false,
    dots: false,
    margin: 20,
    responsive: {
      0: { items: 1 },
      500: { items: 2 },
      769: { items: 3 },
      990: {
        items: 4,
      },
    },
  });
});
