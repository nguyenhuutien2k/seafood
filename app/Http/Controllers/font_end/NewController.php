<?php

namespace App\Http\Controllers\font_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NewController extends Controller
{
    //
    public function listNew(){
        return view('font_end.new');
    }
}
