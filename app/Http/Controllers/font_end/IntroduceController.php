<?php

namespace App\Http\Controllers\font_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IntroduceController extends Controller
{
    //
    public function listIntro(){
        return view('font_end.introduce');
    }
}
