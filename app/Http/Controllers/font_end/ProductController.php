<?php

namespace App\Http\Controllers\font_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function listProduct(){
        return view('font_end.product');
    }

    public function productDetail(){
        return view('font_end.product_detail');
    }
}
