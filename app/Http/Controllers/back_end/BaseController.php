<?php

namespace App\Http\Controllers\back_end;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\Lienhe;

use Darryldecode\Shoppingcart\Cart;

use Darryldecode\Cart\CartCondition;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
class BaseController extends Controller
{

    // Back-end
    public function Admin(){
        return view('backend.admin');
    }
    // end Back-end
}
