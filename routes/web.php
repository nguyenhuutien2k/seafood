<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Font-End 
Route::group(['prefix'=> '/'], function(){
    Route::get('/','font_end\HomeController@index')->name('font.index');
    Route::get('san-pham', 'font_end\ProductController@listProduct')->name('font.product');
    Route::get('tin-tuc', 'font_end\NewController@listNew')->name('font.new');
    Route::get('gioi-thieu', 'font_end\IntroduceController@listIntro')->name('font.intro');
    Route::get('lien-he', 'font_end\ContactController@listContact')->name('font.contact');
    Route::get('chi-tiet', 'font_end\ProductController@productDetail')->name('font.detail');
    Route::get('gio-hang', 'font_end\CartController@listCart')->name('font.cart');
    
});

//Back-End
Route::group(['prefix'=>'/'],function(){
    Route::get('admin', 'back_end\BaseController@Admin')->name('admin');

    // Danh mục
    Route::get('quantridanhmuc','back_end\DanhmucController@Danhmuc')->name('danhmuc');
    Route::get('add-danhmuc', 'back_end\DanhmucController@Adddanhmuc' )->name('adddanhmuc');
    Route::post('add-danhmuc', 'back_end\DanhmucController@Valiadd' )->name('postdanhmuc');
    Route::get('edit-danhmuc/{id}','back_end\DanhmucController@Editdanhmuc')->name('editdanhmuc');
    Route::post('edit-danhmuc/{id}','back_end\DanhmucController@postEditdanhmuc')->name('posteditdanhmuc');
    Route::get('delete-danhmuc/{id}','back_end\DanhmucController@Deletedanhmuc')->name('deletedanhmuc');
});

// Sản phẩm
Route::get('quantrisanpham', 'back_end\SanphamController@Sanpham' )->name('sanpham');
Route::get('add-sanpham', 'back_end\SanphamController@Addsanpham' )->name('addsanpham');
Route::post('add-sanpham', 'back_end\SanphamController@Valiaddsp' )->name('postsanpham');
Route::get('edit-sanpham/{id}', 'back_end\SanphamController@Editsanpham' )->name('editsanpham');
Route::post('edit-sanpham/{id}','back_end\SanphamController@postEditsanpham')->name('posteditsanpham');
Route::get('delete-sanpham/{id}','back_end\SanphamController@Deletesanpham')->name('deletesanpham');

//Login - Logout (User)
Route::group(['prefix'=>'auth'],function(){
    Route::get('dang-ky', 'auth\AuthController@register')->name('auth.register');
    Route::get('dang-nhap', 'auth\AuthController@login')->name('auth.login');
});