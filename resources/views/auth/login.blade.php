@extends('font_end.layouts.master')
@section('title', 'dang nhap')
@section('css')
<link rel="stylesheet" href="./assets/css/login.css" />
@endsection
@section('content')
<div style="max-width: 100%; width: 100%">
    <div>
        <div class="breadcrumb-page">
          <div class="container">
            <ul class="breadcrumb-header">
              <li class="breadcrumb-header-item">
                <a href="#">Trang chủ</a>
              </li>
              <li class="breadcrumb-header-item">
                <a href="#">Tài khoản</a>
              </li>
              <li class="breadcrumb-header-item active">
                <span>Đăng nhập</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="container">
          <div class="form-login">
            <h1>Đăng nhập</h1>
            <label> Email</label>
            <input type="text" />
            <label>Mật khẩu</label>
            <input type="password" />
            <p>Quên mật khẩu nhấn vào <a href="http://">đây</a></p>
            <div><button class="btn">Đăng nhập</button></div>

            <div>
              <button>
                <div></div>
                <div>Đăng nhập với google</div>
              </button>
            </div>
            <div>
              <button>
                <div></div>
                <div>Đăng nhập với facebook</div>
              </button>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection