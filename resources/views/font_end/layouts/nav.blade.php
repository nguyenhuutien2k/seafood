<div class="menu">
          <div class="container">
            <div class="row">
              <div class="col-lg-3">
                <div class="category">
                  <div class="category-drop">
                    <span> danh mục sản phẩm</span>
                  </div>
                  <div id="menu" class="category-content"> 
                  <!-- active -->
                    <ul>
                      <li class="category-content-child">
                        <a href="#"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s1.png?v=44"
                            alt="Ngao-Sò-Ốc"
                          />
                          Ngao-Sò-Ốc</a
                        >
                        <div class="category-content-child-mega">
                          <ul>
                            <li><a href="{{route('font.product')}}">Ngao</a></li>
                            <li><a href="{{route('font.product')}}">Sò</a></li>
                            <li><a href="{{route('font.product')}}">ốc</a></li>
                            <li><a href="{{route('font.product')}}">Hàu</a></li>
                          </ul>
                        </div>
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s2.png?v=44"
                            alt="Bào ngư"
                          />
                          Bào ngư</a
                        >
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s3.png?v=44"
                            alt="Tôm"
                          />
                          Tôm</a
                        >
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s4.png?v=44"
                            alt="Mực"
                          />Mực
                        </a>
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s5.png?v=44"
                            alt="Cá"
                          />
                          Cá</a
                        >
                        <div class="category-content-child-mega">
                          <ul>
                            <li>
                              <a href="{{route('font.product')}}">Cá ngừ đại dương</a>
                            </li>
                            <li><a href="{{route('font.product')}}">Cá hồi</a></li>
                          </ul>
                        </div>
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s6.png?v=44"
                            alt="Cua-ghẹ"
                          />Cua-ghẹ</a
                        >
                        <div class="category-content-child-mega">
                          <ul>
                            <li><a href="{{route('font.product')}}">Cua </a></li>
                            <li><a href="{{route('font.product')}}">Ghẹ</a></li>
                          </ul>
                        </div>
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s7.png?v=44"
                            alt="Sứa"
                          />Sứa</a
                        >
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s8.png?v=44"
                            alt="Hải sản đóng hộp"
                          />Hải sản đóng hộp - chế biến</a
                        >
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s8.png?v=44"
                            alt="Sản phẩm khô"
                          />Sản phẩm khô</a
                        >
                        <div class="category-content-child-mega">
                          <ul>
                            <li>
                              <a href="{{route('font.product')}}">Hải sản khô</a>
                            </li>
                          </ul>
                        </div>
                      </li>
                      <li class="category-content-child">
                        <a href="{{route('font.product')}}"
                          ><img
                            src="http://theme.hstatic.net/1000181509/1000452638/14/s8.png?v=44"
                            alt="Sản phẩm khác"
                          />
                          Sản phẩm khác</a
                        >
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="menu-main">
                  <ul>
                    <li class="menu-child">
                      <a href="{{route('font.index')}}"> Trang chủ</a>
                    </li>
                    <li class="menu-child" class="{{'font/san-pham' ==request()->path() ? 'active' : ''}}">
                      <a href="{{route('font.product')}}"> Sản phẩm</a>
                      <!-- route() -->
                    </li>
                    <li class="menu-child">
                      <a href="{{route('font.new')}}"> Tin tức </a>
                      <div class="menu-child-mega">
                        <ul>
                          <li>
                            <a class="button-hover" href="{{route('font.new')}}"
                              >Tin hải sản
                            </a>
                          </li>
                          <li>
                            <a class="button-hover" href="{{route('font.new')}}"
                              >Món ngon mỗi ngày</a
                            >
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="menu-child">
                      <a href="{{route('font.intro')}}"> Giới thiệu</a>
                    </li>
                    <li class="menu-child">
                      <a href="{{route('font.contact')}}"> Liên hệ</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>