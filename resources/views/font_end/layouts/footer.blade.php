<div class="footer-wrapper">
        <div class="container">
          <div class="footer-top">
            <div class="row">
              <div class="col-lg-3 col-md-6">
                <div class="footer-block">
                  <h4>thông tin</h4>
                  <div class="block-content">
                    <ul>
                      <li><a href="#">Tìm kiếm </a></li>
                      <li><a href="#">Giới thiệu </a></li>
                      <li><a href="#">Liên hệ </a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="footer-block">
                  <h4>hỗ trợ</h4>
                  <div class="block-content">
                    <ul>
                      <li><a href="#">Thanh toán giao nhận </a></li>
                      <li><a href="#">Chính sách đổi trả</a></li>
                      <li><a href="#"> Chính sách bảo mật </a></li>
                      <li><a href="#"> Hướng dẫn mua hàng </a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="footer-block">
                  <h4>đăng ký nhận bản tin</h4>
                  <div class="block-content">
                    <div class="des-block-content">
                      đăng ký thành viên để nhận bản tin mỗi ngày
                    </div>
                    <input type="text" placeholder="Email của bạn " />
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="footer-block">
                  <h4>theo dõi trên fanpage</h4>
                  <div class="block-content">
                    <div class="fanpage-pinky">
                      <div>
                        <div class="fanpage-pinky-image">
                          <img
                            alt="haravan"
                            src="https://scontent.fhan5-2.fna.fbcdn.net/v/t1.0-0/s552x414/114218776_2059247664209255_7571311067200836108_o.png?_nc_cat=110&_nc_sid=dd9801&_nc_ohc=k94oECUZmG8AX_zxQdd&_nc_ht=scontent.fhan5-2.fna&oh=d16c5db46f9286799bf477a6f514f05a&oe=5F9576C4"
                          />
                        </div>
                        <div class="fanpage-pinky-top">
                          <div class="pinky-top"></div>
                          <div class="pinky-number">
                            <div class="d-flex">
                              <a
                                href="https://www.facebook.com/haravan.official"
                              >
                                <img
                                  alt="haravan"
                                  src="https://scontent.fhan5-5.fna.fbcdn.net/v/t1.0-1/cp0/p80x80/12122825_740319142768787_1875715644858830276_n.png?_nc_cat=101&_nc_sid=dbb9e7&_nc_ohc=8beOePcUYvgAX_pIUCW&_nc_ht=scontent.fhan5-5.fna&oh=05c2ea211cc716b49cdb1efde9f41d32&oe=5F97B11D"
                              /></a>
                              <div class="haravan">
                                <div class="d-flex">
                                  <a
                                    href="https://www.facebook.com/haravan.official"
                                  >
                                    Haravan
                                  </a>
                                  <span> </span>
                                </div>
                                <div>71.004 luợt thích</div>
                              </div>
                            </div>
                          </div>
                          <div class="pinky-button">
                            <span class="img-fb">
                              <a href="#"> <i></i>Thích Trang </a>
                            </span>
                            <span class="img-fb">
                              <a href="#"> <i></i>Chia sẻ </a>
                            </span>
                          </div>
                        </div>
                      </div>

                      <div class="pinky-p">
                        <p>
                          Hãy là người đầu tiên trong số bạn bè của bạn thích
                          nội dung này
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!--  -->
          <div class="footer-info">
            <div class="row">
              <div class="footer-address col-xl-9 col-lg-12">
                <div>
                  <p>Công ty cổ phần công nghệ Haravan</p>
                  <p>
                    Địa chỉ: Tòa nhà IT business, 55 Van Coi, phường 7, quận Tân
                    Bình, Tp.Hồ Chí Minh
                  </p>
                  <p>Số điện thoại : 1900 636 099</p>
                  <p>Số di động: 0122 7620 462</p>
                  <p>Email: luan951994@gmail.com</p>
                </div>
              </div>

              <div class="col-xl-3 col-lg-12">
                <div class="share-box">
                  <div class="copy-right">
                    <p>
                      &#169; 2020. Powered by <a href="http://"> Hararvan</a>
                    </p>
                    <div class="share-icon">
                      <ul>
                        <li>
                          <a href="https://">
                            <span class="fa fa-twitter"></span
                          ></a>
                        </li>
                        <li>
                          <a href="https://">
                            <span class="fa fa-facebook"></span
                          ></a>
                        </li>
                        <li>
                          <a href="https://">
                            <span class="fa fa-google-plus"></span
                          ></a>
                        </li>
                        <li>
                          <a href="https://">
                            <span class="fa fa-youtube"></span
                          ></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>