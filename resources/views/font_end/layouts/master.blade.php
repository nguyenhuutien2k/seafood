<!DOCTYPE html>
<html lang="en">
  <head>
    <title>@yield('title')</title>
    <meta
      name="description"
      content="Cửa hàng chuyên cung cấp các loại hải sản tươi ngon, tiện lợi"
    />
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <base href="{{ asset('') }}">
    <!-- icon  -->
    <link
      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      rel="stylesheet"
      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
      crossorigin="anonymous"
    />
    <!-- Bootstrap CSS -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />
    <!-- thư viện owl -->
    <link
      rel="stylesheet"
      href="./assets/plugin/OwlCarousel2-2.3.4/dist/assets/owl.carousel.css"
    />
    <link
      rel="stylesheet"
      href="./assets/plugin/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css"
    />
    <!--  -->
    <link rel="stylesheet" href="./assets/css/menu.css" />
    <link rel="stylesheet" href="./assets/css/index.css" />
    <link rel="stylesheet" href="./assets/css/footer.css" />
    <link rel="stylesheet" href="./assets/css/modal.css" />
    @yield('css')
  </head>
  <body>
    <div id="opacity" class="opacity-body"></div>
    <div>
      <!-- header -->
        @include('font_end.layouts.header')
      <!-- body -->
        @yield('content')
      <!-- footer -->
      @include('font_end.layouts.footer')

      <!-- nút top scroll -->
      <a href="http://" id="topScroll">
        <div>Top</div>
      </a>
      <!--  -->
      
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <!-- thư viện owl -->
    <script src="./assets/plugin/OwlCarousel2-2.3.4/docs/assets/vendors/jquery.min.js"></script>
    <script src="./assets/plugin/OwlCarousel2-2.3.4/dist/owl.carousel.js"></script>
    <!--  -->
    @yield('script')    
    <script src="./assets/js/slider_index.js"></script>
    <script src="./assets/js/menu.js"></script>
    <script src="./assets/js/input_number.js"></script>
    <script src="./assets/js/modalicon.js"></script>
    <script src="./assets/js/product.js"></script>
  </body>
</html>
