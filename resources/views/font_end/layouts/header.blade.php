<div class="pt-4">
        <!-- menu mobile -->
        <div class="header-mobile pb-3">
          <div class="container">
            <div class="row">
              <div class="col-md-1 col-sm-1 col-2">
                <div class="menu-icon">
                  <button id="open-menu">
                    <span><i class="fa fa-bars" aria-hidden="true"></i></span>
                  </button>
                  <div id="menu-mobile-left" class="modal1">
                    <div class="menu-mobile-content">
                      <div>
                        <button class="close">&times;</button>
                        <div class="menu-mobile-wrap">
                          <div class="menu-mobile-top">
                            <div class="user-icon-mobile">
                              <img
                                src="https://theme.hstatic.net/1000181509/1000452638/14/user_mobile.png?v=44"
                                alt="tài khoản người dùng"
                              />
                            </div>
                            <div class="login-mobile">
                              <ul class="breadcrumb-header">
                                <li class="breadcrumb-header-item">
                                  <a href="./login1.html">Đăng nhập</a>
                                </li>
                                <li class="breadcrumb-header-item">
                                  <a href="./login.html">Đăng ký</a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="menu-mobile-bottom">
                            <ul>
                              <li><a href="./index.html">Trang chủ</a></li>
                              <li><a href="./product.html">Sản phẩm</a></li>
                              <li class="menu-mobile-drop">
                                <a href="./news.html">Tin tức </a
                                ><span id="drop-menu"></span>
                                <ul>
                                  <li>
                                    <a href="./news.html">Tin hải sản </a>
                                  </li>
                                  <li>
                                    <a href="./news.html">Món ngon mỗi ngày </a>
                                  </li>
                                </ul>
                              </li>
                              <li>
                                <a href="./introduce.html">Giới thiệu </a>
                              </li>
                              <li><a href="./contact.html">Liên hệ </a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-11 col-sm-11 col-10">
                <div class="mobile-wrap-menu">
                  <div class="logo-mobile">
                    <a href="{{route('font.index')}}">
                      <img
                        src="http://theme.hstatic.net/1000181509/1000452638/14/logo.png?v=44"
                        alt="logo sea food"
                    /></a>
                  </div>
                  <div class="cart-header">
                    <a href="#">
                      <div class="cart-icon"></div>
                      <div class="cart-bag"><span>2</span></div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="input-mobile">
              <div class="input-search-mobile">
                <input type="text" placeholder="Nhập từ khóa tìm kiếm" />
                <div class="input-text-mobile">
                  <button class="button">
                    <i class="fa fa-search" aria-hidden="true"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--  -->
        <!-- 1 -->
        <div class="top-logo">
          <div class="container">
            <div class="row pb-4">
              <div class="col-lg-3">
                <div class="logo">
                  <a href="{{route('font.index')}}"
                    ><img
                      src="http://theme.hstatic.net/1000181509/1000452638/14/logo.png?v=44"
                      alt="logo sea food"
                  /></a>
                </div>
              </div>
              <div class="col-xl-6 col-lg-5">
                <div class="input_search">
                  <input type="text" placeholder="Nhập từ khóa tìm kiếm" />
                  <div class="input-text">
                    <button class="button">Tìm ngay</button>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4">
                <div class="user-cart-box">
                  <div class="user_login clear-fix mr-4">
                    <div class="user-icon"></div>
                    <div class="user-text mt-2">
                      <strong>Tài khoản</strong>
                    </div>
                    <div class="user-box">
                      <div class="arrow"></div>
                      <div class="user-box-list">
                        <ul>
                          <li><a href="{{route('auth.login')}}">Đăng nhập</a></li>
                          <li><a href="{{route('auth.register')}}">Đăng ký</a></li>
                          <li><a href="http://">Facebook</a></li>
                          <li><a href="http://">Google</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="cart_link clearfix">
                    <a href="{{route('font.cart')}}">
                      <div class="cart-icon"></div>
                      <div class="cart-text">
                        <strong>Giỏ hàng <span>2</span></strong>
                        <span>340,000</span>
                      </div>
                    </a>

                    <div class="cart-box">
                      <div class="arrow"></div>
                      <div class="item1 active">
                        <div class="cart-box-product">
                          <div class="detail-product-items">
                            <ul>
                              <li class="active">
                                <div>
                                  <div class="cart-item-image">
                                    <img
                                      src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_small.jpg"
                                      alt="Cồi sò điệp"
                                    />
                                  </div>
                                  <div class="cart-item-info">
                                    <p class="title">
                                      <a href="./product_detail.html"
                                        >Cồi sò điệp</a
                                      >
                                    </p>
                                    <div class="price-info">
                                      <span>1</span><span>128,000</span>
                                    </div>
                                    <div class="clear-item"></div>
                                  </div>
                                </div>
                              </li>
                              <li class="active">
                                <div>
                                  <div class="cart-item-image">
                                    <img
                                      src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_small.jpg"
                                      alt="Cồi sò điệp"
                                    />
                                  </div>
                                  <div class="cart-item-info">
                                    <p class="title">
                                      <a href="./product_detail.html"
                                        >Cồi sò điệp</a
                                      >
                                    </p>
                                    <div class="price-info">
                                      <span>1</span><span>128,000</span>
                                    </div>
                                    <div class="clear-item"></div>
                                  </div>
                                </div>
                              </li>
                              <li class="active">
                                <div>
                                  <div class="cart-item-image">
                                    <img
                                      src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_small.jpg"
                                      alt="Cồi sò điệp"
                                    />
                                  </div>
                                  <div class="cart-item-info">
                                    <p class="title">
                                      <a href="./product_detail.html"
                                        >Cồi sò điệp</a
                                      >
                                    </p>
                                    <div class="price-info">
                                      <span>1</span><span>128,000</span>
                                    </div>
                                    <div class="clear-item"></div>
                                  </div>
                                </div>
                              </li>
                              <li class="active">
                                <div>
                                  <div class="cart-item-image">
                                    <img
                                      src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_small.jpg"
                                      alt="Cồi sò điệp"
                                    />
                                  </div>
                                  <div class="cart-item-info">
                                    <p class="title">
                                      <a href="./product_detail.html"
                                        >Cồi sò điệp</a
                                      >
                                    </p>
                                    <div class="price-info">
                                      <span>1</span><span>128,000</span>
                                    </div>
                                    <div class="clear-item"></div>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                          <div class="total-items">
                            <span>Tổng tiền:</span><span>512,000</span>
                          </div>
                        </div>
                        <div class="cart-box-button">
                          <a href="{{route('font.cart')}}">Xem giỏ hàng </a
                          ><a href="http://">Thanh toán</a>
                        </div>
                      </div>
                      <div class="cart-empty">
                        Giỏ hàng của bạn chưa có sản phẩm nào
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- 2 -->
        @include('font_end.layouts.nav')
</div>