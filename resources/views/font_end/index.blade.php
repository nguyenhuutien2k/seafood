@extends('font_end.layouts.master')
@section('title', 'Seafood - Cửa hàng hải sản')
@section('content')
<div class="pt-4">
<!-- 3 -->
<div class="banner-slider-index">
          <div class="container">
            <div class="row justify-content-end">
              <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                <div class="slider">
                  <ul class="owl-carousel owl-theme" id="owl-slider">
                    <li class="slide-show">
                      <a href="#">
                        <img
                          alt="A World Cup cake"
                          src="http://theme.hstatic.net/1000181509/1000452638/14/slider_1.jpg?v=44"
                        />
                      </a>
                    </li>
                    <li class="slide-show">
                      <a href="#">
                        <img
                          alt="A World Cup cake"
                          src="http://theme.hstatic.net/1000181509/1000452638/14/slider_2.jpg?v=44"
                        />
                      </a>
                    </li>
                    <li class="slide-show">
                      <a href="#">
                        <img
                          alt="A World Cup cake"
                          src="http://theme.hstatic.net/1000181509/1000452638/14/slider_3.jpg?v=44"
                        />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- 4 -->
        <div class="mt-4">
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-sm-6 col-12">
                <div class="service-item">
                  <div class="service-icon">
                    <img
                      src="https://theme.hstatic.net/1000181509/1000452638/14/icon_1.png?v=44"
                      alt="Miễn phí giao hàng"
                    />
                  </div>
                  <div class="service-text">
                    <span>miễn phí giao hàng</span>
                    <span>Cho hóa đơn từ 450,000</span>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-12">
                <div class="service-item">
                  <div class="service-icon">
                    <img
                      src="https://theme.hstatic.net/1000181509/1000452638/14/icon_2.png?v=44"
                      alt="Miễn phí giao hàng"
                    />
                  </div>
                  <div class="service-text">
                    <span>giao hàng trong ngày </span>
                    <span>Với tất cả hóa đơn</span>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="service-item">
                  <div class="service-icon">
                    <img
                      src="https://theme.hstatic.net/1000181509/1000452638/14/icon_3.png?v=44"
                      alt="Miễn phí giao hàng"
                    />
                  </div>
                  <div class="service-text">
                    <span> sản phẩm an toàn</span>
                    <span>Cam kết chất lượng </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>
<!-- body -->
<div class="mt-4">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 body-right">
              <!-- 1 -->
              <div class="sidebar-banner">
                <a href="#">
                  <img
                    src="http://theme.hstatic.net/1000181509/1000452638/14/banner_sidebar_img_1.jpg?v=44"
                    alt=""
                  />
                  <div class="shadow-banner"></div>
                </a>
              </div>
              <!-- 2 -->
              <div class="sidebar-banner mt-4">
                <a href="#">
                  <img
                    src="http://theme.hstatic.net/1000181509/1000452638/14/banner_sidebar_img_2.jpg?v=44"
                    alt=""
                  />
                  <div class="shadow-banner"></div>
                </a>
              </div>
              <!-- 3 -->
              <div class="blog-banner mt-4">
                <h2 title="bài viết mới">bài viết mới</h2>
                <div class="blog-content">
                  <ul>
                    <!-- 1 -->
                    <li class="blog-item">
                      <div>
                        <div class="blog-image">
                          <a href="#"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/cach-lam-lau-kim-chi-hai-san_eca4b6aa865748c2890ae5ca90f7649b_medium.jpg"
                              alt="cách làm nẩu kim chi hải sản"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h3>
                            <a href="#"> Cách làm lẩu kim chi hải sản </a>
                          </h3>
                          <div class="blog-info">
                            <div>
                              <i
                                class="fa fa-comments-o blue_light"
                                aria-hidden="true"
                              ></i>
                              0 bình luận
                            </div>
                            <div>
                              <i
                                class="fa fa-calendar blue_light"
                                aria-hidden="true"
                              ></i>
                              10/5/2017
                            </div>
                          </div>
                          <div class="blog-des">
                            Lẩu kim chi chua cay lại thêm vị ngọt của hải sản,
                            cực kỳ tuyệt vời luôn nhé!
                          </div>
                          <div class="next-info">
                            <a href="#" class="button"> đọc tiếp &#8811;</a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <!-- 2 -->
                    <li class="blog-item">
                      <div>
                        <div class="blog-image">
                          <a href="#"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/muc-chien-xu-sot-wasabi-an-mot-mieng-la-me-man-ngay_011f174901b547af81ac7162084b00d7_medium.jpg"
                              alt="Mực chiên xù sốt wasabi ăn một miếng và mê mẩn ngay"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h3>
                            <a
                              href="#"
                              title="Mực chiên xù sốt wasabi ăn một miếng và mê mẩn ngay"
                            >
                              Mực chiên xù sốt wasabi ăn một miếng và mê mẩn
                              ngay
                            </a>
                          </h3>
                          <div class="blog-info">
                            <div>
                              <i
                                class="fa fa-comments-o blue_light"
                                aria-hidden="true"
                              ></i>
                              0 bình luận
                            </div>
                            <div>
                              <i
                                class="fa fa-calendar blue_light"
                                aria-hidden="true"
                              ></i>
                              10/5/2017
                            </div>
                          </div>
                          <div class="blog-des">
                            Mực chiên xù thì chẳng ai lạ lẫm gì nữa rồi, nhưng
                            mực chiên xóc với sốt wasabi cay cay...
                          </div>
                          <div class="next-info">
                            <a href="#" class="button"> đọc tiếp &#8811;</a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <!-- 3 -->
                    <li class="blog-item">
                      <div>
                        <div class="blog-image">
                          <a href="#"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/tom-hap-bia_medium.jpg"
                              alt="Tôm bia hấp"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h3>
                            <a href="#" title="Tôm bia hấp"> Tôm bia hấp </a>
                          </h3>
                          <div class="blog-info">
                            <div>
                              <i
                                class="fa fa-comments-o blue_light"
                                aria-hidden="true"
                              ></i>
                              0 bình luận
                            </div>
                            <div>
                              <i
                                class="fa fa-calendar blue_light"
                                aria-hidden="true"
                              ></i>
                              10/5/2017
                            </div>
                          </div>
                          <div class="blog-des">
                            Nguyên liệu làm món tôm hấp bia: - Tôm sú: 1kg .
                            Chọn tôm không - Bia : 1 lon- Sả :2...
                          </div>
                          <div class="next-info">
                            <a href="#" class="button"> đọc tiếp &#8811;</a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <!-- 4 -->
                    <li class="blog-item">
                      <div>
                        <div class="blog-image">
                          <a href="#"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/muc-om-nuoc-dua-hat-sen_medium.jpg"
                              alt="Mực om nước dừa hạt sen"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h3>
                            <a href="#" title="Mực om nước dừa hạt sen">
                              Mực om nước dừa hạt sen</a
                            >
                          </h3>
                          <div class="blog-info">
                            <div>
                              <i
                                class="fa fa-comments-o blue_light"
                                aria-hidden="true"
                              ></i>
                              0 bình luận
                            </div>
                            <div>
                              <i
                                class="fa fa-calendar blue_light"
                                aria-hidden="true"
                              ></i>
                              10/5/2017
                            </div>
                          </div>
                          <div class="blog-des">
                            500g mực ống tươi làm sạch và để nguyên con 150g hạt
                            sen 100g giò sống 150ml nước dừa Gia...
                          </div>
                          <div class="next-info">
                            <a href="#" class="button"> đọc tiếp &#8811;</a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <!-- 5 -->
                    <li class="blog-item">
                      <div>
                        <div class="blog-image">
                          <a href="#"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/cach-lam-muc-tam-bot-chien-gion-thom-ngon_medium.jpg"
                              alt="Cách làm mực tẩm bột chiên giòn thơm ngon"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h3>
                            <a
                              href="#"
                              title="Cách làm mực tẩm bột chiên giòn thơm ngon"
                            >
                              Cách làm mực tẩm bột chiên giòn thơm ngon
                            </a>
                          </h3>
                          <div class="blog-info">
                            <div>
                              <i
                                class="fa fa-comments-o blue_light"
                                aria-hidden="true"
                              ></i>
                              0 bình luận
                            </div>
                            <div>
                              <i
                                class="fa fa-calendar blue_light"
                                aria-hidden="true"
                              ></i>
                              10/5/2017
                            </div>
                          </div>
                          <div class="blog-des">
                            Có rất nhiều món ăn được chế biến từ những nguyên
                            liệu khá phổ biến và thường sẵn có trong ...
                          </div>
                          <div class="next-info">
                            <a href="#" class="button"> đọc tiếp &#8811;</a>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <!--  -->
            </div>
            <div class="col-lg-9">
              <!-- 1 -->
              <div>
                <div class="product-banner">
                  <a href="#">
                    <img
                      src="http://theme.hstatic.net/1000181509/1000452638/14/image-product-home-1.jpg?v=44"
                      alt="banner1"
                    />
                    <div class="shadow-banner"></div>
                  </a>
                </div>

                <div class="product-grid">
                  <div class="section-heading">
                    <h2><span>Tôm</span></h2>
                  </div>
                  <div class="product-list">
                    <ul class="row" style="display: flex !important;">
                      <!-- 1 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="#"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="#" title="Cồi sò điệp"> Cồi sò điệp</a>
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/hau-sua-1_medium.jpg"
                                alt="Hàu sữa"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html">Hàu sữa</a>
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/ngao-2-coi-1_medium.jpg"
                                alt="ngao 2 cồi"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="ngao 2 cồi"
                              >
                                Ngao 2 cồi</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="/product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/oc-voi-6_medium.jpg"
                                alt="Ốc vòi"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Ốc vòi">
                                Ốc vòi</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 5 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/so-diep-1_medium.jpg"
                                alt="Sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Sò điệp">
                                Sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 6 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/so-mai-1_medium.jpg"
                                alt="Sò mai"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Sò mai"
                                >Sò mai</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 7 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/tom_cang_xanh_01_medium.jpg"
                                alt="Tôm càng xanh "
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Tôm càng xanh"
                              >
                                Tôm càng xanh</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 8 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/tom_hum_bong_01_medium.jpg"
                                alt="Tôm hùm bông"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Tôm hùm bông"
                              >
                                Tôm hùm bông</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="show-more">
                    <span
                      >Mời bạn <a href="#"> Xem thêm sản phẩm tôm </a>khác</span
                    >
                  </div>
                </div>
              </div>
              <!-- 2 -->
              <div>
                <div class="product-banner">
                  <a href="#">
                    <img
                      src="https://theme.hstatic.net/1000181509/1000452638/14/image-product-home-2.jpg?v=44"
                      alt="banner2"
                    />
                    <div class="shadow-banner"></div>
                  </a>
                </div>
                <div class="product-grid">
                  <div class="section-heading">
                    <h2><span>Tôm</span></h2>
                  </div>
                  <div class="product-list">
                    <ul class="row" style="display: flex !important;">
                      <!-- 1 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="#"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="#" title="Cồi sò điệp"> Cồi sò điệp</a>
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/hau-sua-1_medium.jpg"
                                alt="Hàu sữa"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Hàu sữa">
                                Hàu sữa</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/ngao-2-coi-1_medium.jpg"
                                alt="Ngao 2 cồi"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Ngao 2 cồi"
                              >
                                Ngao 2 cồi</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/oc-voi-6_medium.jpg"
                                alt="Ốc vòi"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Ốc vòi">
                                Ốc vòi</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 5 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/so-diep-1_medium.jpg"
                                alt="Sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Sò điệp"
                                >Sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 6 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/so-mai-1_medium.jpg"
                                alt="Sò mai"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Sò mai">
                                Sò mai</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 7 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="#"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/tom_cang_xanh_01_medium.jpg"
                                alt="Tôm càng xanh"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Tôm càng xanh"
                              >
                                Tôm càng xanh</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 8 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/tom_hum_bong_01_medium.jpg"
                                alt="Tôm hùm bông"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Tôm hùm bông"
                              >
                                Tôm hùm bông</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="show-more">
                    <span
                      >Mời bạn <a href="#"> Xem thêm sản phẩm tôm </a>khác</span
                    >
                  </div>
                </div>
              </div>
              <!-- 3 -->
              <div>
                <div class="product-banner">
                  <a href="#">
                    <img
                      src="https://theme.hstatic.net/1000181509/1000452638/14/image-product-home-3.jpg?v=44"
                      alt="banner3"
                    />
                    <div class="shadow-banner"></div>
                  </a>
                </div>
                <div class="product-grid">
                  <div class="section-heading">
                    <h2><span>Tôm</span></h2>
                  </div>
                  <div class="product-list">
                    <ul class="row" style="display: flex !important;">
                      <!-- 1 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="#"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="#" title="Cồi sò điệp"> Cồi sò điệp</a>
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/hau-sua-1_medium.jpg"
                                alt="Hàu sữa"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Hàu sữa"
                                >Hàu sữa</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/ngao-2-coi-1_medium.jpg"
                                alt="Ngao 2 cồi"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Ngao 2 cồi"
                              >
                                Ngao 2 cồi</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/oc-voi-6_medium.jpg"
                                alt="Ốc vòi"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Ốc vòi">
                                Ốc vòi</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 5 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="#"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/so-diep-1_medium.jpg"
                                alt="Sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Sò điệp"
                                >Sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 6 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/so-mai-1_medium.jpg"
                                alt="Sò mai"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a href="./product_detail.html" title="Sò mai">
                                Sò mai</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 7 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/tom_cang_xanh_01_medium.jpg"
                                alt="Tôm càng xanh"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Tôm càng xanh"
                                >Tôm càng xanh</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 8 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/tom_hum_bong_01_medium.jpg"
                                alt="Tôm hùm bông"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a href="#" class="detail-eye">
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Tôm hùm bông"
                              >
                                Tôm hùm bông</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="show-more">
                    <span
                      >Mời bạn <a href="#"> Xem thêm sản phẩm tôm </a>khác</span
                    >
                  </div>
                </div>
              </div>
              <!--  -->
            </div>
          </div>
          <div>
            <div class="section-heading">
              <h2><span>Tư vấn món ngon</span></h2>
            </div>
            <div class="blog-index">
              <ul class="owl-carousel owl-theme" id="blog-index-list">
                <!-- 1 -->
                <li class="blog-index-item">
                  <div>
                    <div class="blog-image">
                      <a href="#"
                        ><img
                          src="https://file.hstatic.net/1000181509/article/cach-lam-lau-kim-chi-hai-san_eca4b6aa865748c2890ae5ca90f7649b_medium.jpg"
                          alt="cách làm nẩu kim chi hải sản"
                      /></a>
                    </div>
                    <div class="blog-item-child">
                      <h3>
                        <a href="#"> Cách làm lẩu kim chi hải sản </a>
                      </h3>
                      <div class="blog-info">
                        <div>
                          <i
                            class="fa fa-comments-o blue_light"
                            aria-hidden="true"
                          ></i>
                          0 bình luận
                        </div>
                        <div>
                          <i
                            class="fa fa-calendar blue_light"
                            aria-hidden="true"
                          ></i>
                          10/5/2017
                        </div>
                      </div>
                      <div class="blog-des">
                        Lẩu kim chi chua cay lại thêm vị ngọt của hải sản, cực
                        kỳ tuyệt vời luôn nhé!
                      </div>
                      <div class="next-info">
                        <a href="#" class="button"> đọc tiếp &#8811;</a>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- 2 -->
                <li class="blog-index-item">
                  <div>
                    <div class="blog-image">
                      <a href="./detail_news.html"
                        ><img
                          src="https://file.hstatic.net/1000181509/article/muc-chien-xu-sot-wasabi-an-mot-mieng-la-me-man-ngay_011f174901b547af81ac7162084b00d7_large.jpg"
                          alt="Mực chiên xù sốt wasabi ăn một miếng và mê mẩn ngay"
                      /></a>
                    </div>
                    <div class="blog-item-child">
                      <h3>
                        <a href="./detail_news.html">
                          Mực chiên xù sốt wasabi ăn một miếng và mê mẩn ngay
                        </a>
                      </h3>
                      <div class="blog-info">
                        <div>
                          <i
                            class="fa fa-comments-o blue_light"
                            aria-hidden="true"
                          ></i>
                          0 bình luận
                        </div>
                        <div>
                          <i
                            class="fa fa-calendar blue_light"
                            aria-hidden="true"
                          ></i>
                          10/5/2017
                        </div>
                      </div>
                      <div class="blog-des">
                        Lẩu kim chi chua cay lại thêm vị ngọt của hải sản, cực
                        kỳ tuyệt vời luôn nhé!
                      </div>
                      <div class="next-info">
                        <a href="#" class="button"> đọc tiếp &#8811;</a>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- 3 -->
                <li class="blog-index-item">
                  <div>
                    <div class="blog-image">
                      <a href="./detail_news.html"
                        ><img
                          src="https://file.hstatic.net/1000181509/article/cach-lam-muc-tam-bot-chien-gion-thom-ngon_large.jpg"
                          alt="Cách làm mực tẩm bột chiên giòn thơm ngon"
                      /></a>
                    </div>
                    <div class="blog-item-child">
                      <h3>
                        <a href="./detail_news.html"
                          >Cách làm mực tẩm bột chiên giòn thơm ngon
                        </a>
                      </h3>
                      <div class="blog-info">
                        <div>
                          <i
                            class="fa fa-comments-o blue_light"
                            aria-hidden="true"
                          ></i>
                          0 bình luận
                        </div>
                        <div>
                          <i
                            class="fa fa-calendar blue_light"
                            aria-hidden="true"
                          ></i>
                          10/5/2017
                        </div>
                      </div>
                      <div class="blog-des">
                        Lẩu kim chi chua cay lại thêm vị ngọt của hải sản, cực
                        kỳ tuyệt vời luôn nhé!
                      </div>
                      <div class="next-info">
                        <a href="#" class="button"> đọc tiếp &#8811;</a>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- 4 -->
                <li class="blog-index-item">
                  <div>
                    <div class="blog-image">
                      <a href="./detail_news.html"
                        ><img
                          src="https://file.hstatic.net/1000181509/article/canh-rong-bien-thit-bo_large.jpg"
                          alt="Canh rong biển thịt bò"
                      /></a>
                    </div>
                    <div class="blog-item-child">
                      <h3>
                        <a href="./detail_news.html">Canh rong biển thịt bò </a>
                      </h3>
                      <div class="blog-info">
                        <div>
                          <i
                            class="fa fa-comments-o blue_light"
                            aria-hidden="true"
                          ></i>
                          0 bình luận
                        </div>
                        <div>
                          <i
                            class="fa fa-calendar blue_light"
                            aria-hidden="true"
                          ></i>
                          10/5/2017
                        </div>
                      </div>
                      <div class="blog-des">
                        Lẩu kim chi chua cay lại thêm vị ngọt của hải sản, cực
                        kỳ tuyệt vời luôn nhé!
                      </div>
                      <div class="next-info">
                        <a href="#" class="button"> đọc tiếp &#8811;</a>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- 5 -->
                <li class="blog-index-item">
                  <div>
                    <div class="blog-image">
                      <a href="./detail_news.html"
                        ><img
                          src="https://file.hstatic.net/1000181509/article/muc-om-nuoc-dua-hat-sen_large.jpg"
                          alt="Mực om nước dừa hạt sen"
                      /></a>
                    </div>
                    <div class="blog-item-child">
                      <h3>
                        <a href="./detail_news.html">
                          Mực om nước dừa hạt sen</a
                        >
                      </h3>
                      <div class="blog-info">
                        <div>
                          <i
                            class="fa fa-comments-o blue_light"
                            aria-hidden="true"
                          ></i>
                          0 bình luận
                        </div>
                        <div>
                          <i
                            class="fa fa-calendar blue_light"
                            aria-hidden="true"
                          ></i>
                          10/5/2017
                        </div>
                      </div>
                      <div class="blog-des">
                        Lẩu kim chi chua cay lại thêm vị ngọt của hải sản, cực
                        kỳ tuyệt vời luôn nhé!
                      </div>
                      <div class="next-info">
                        <a href="#" class="button"> đọc tiếp &#8811;</a>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- 6 -->
                <li class="blog-index-item">
                  <div>
                    <div class="blog-image">
                      <a href="./detail_news.html"
                        ><img
                          src="https://file.hstatic.net/1000181509/article/tom-hap-bia_large.jpg"
                          alt="Tôm hấp bia"
                      /></a>
                    </div>
                    <div class="blog-item-child">
                      <h3>
                        <a href="./detail_news.html"> Tôm hấp bia </a>
                      </h3>
                      <div class="blog-info">
                        <div>
                          <i
                            class="fa fa-comments-o blue_light"
                            aria-hidden="true"
                          ></i>
                          0 bình luận
                        </div>
                        <div>
                          <i
                            class="fa fa-calendar blue_light"
                            aria-hidden="true"
                          ></i>
                          10/5/2017
                        </div>
                      </div>
                      <div class="blog-des">
                        Lẩu kim chi chua cay lại thêm vị ngọt của hải sản, cực
                        kỳ tuyệt vời luôn nhé!
                      </div>
                      <div class="next-info">
                        <a href="#" class="button"> đọc tiếp &#8811;</a>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- 7 -->
                <li class="blog-index-item">
                  <div>
                    <div class="blog-image">
                      <a href="./detail_news.html"
                        ><img
                          src="https://file.hstatic.net/1000181509/article/ca-hoi-nuong-giay-bac_large.jpg"
                          alt="Cá hồi nướng giấy bạc"
                      /></a>
                    </div>
                    <div class="blog-item-child">
                      <h3>
                        <a href="./detail_news.html"> Cá hồi nướng giấy bạc</a>
                      </h3>
                      <div class="blog-info">
                        <div>
                          <i
                            class="fa fa-comments-o blue_light"
                            aria-hidden="true"
                          ></i>
                          0 bình luận
                        </div>
                        <div>
                          <i
                            class="fa fa-calendar blue_light"
                            aria-hidden="true"
                          ></i>
                          10/5/2017
                        </div>
                      </div>
                      <div class="blog-des">
                        Lẩu kim chi chua cay lại thêm vị ngọt của hải sản, cực
                        kỳ tuyệt vời luôn nhé!
                      </div>
                      <div class="next-info">
                        <a href="#" class="button"> đọc tiếp &#8811;</a>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- 8 -->
                <li class="blog-index-item">
                  <div>
                    <div class="blog-image">
                      <a href="./detail_news.html"
                        ><img
                          src="https://file.hstatic.net/1000181509/article/cach-che-bien-mon-ngon-tu-kho-ca-dua_large.jpg"
                          alt="Cách chế biến món ngon từ khô cá dứa "
                      /></a>
                    </div>
                    <div class="blog-item-child">
                      <h3>
                        <a href="/detail_news.html">
                          Cách chế biến món ngon từ khô cá dứa
                        </a>
                      </h3>
                      <div class="blog-info">
                        <div>
                          <i
                            class="fa fa-comments-o blue_light"
                            aria-hidden="true"
                          ></i>
                          0 bình luận
                        </div>
                        <div>
                          <i
                            class="fa fa-calendar blue_light"
                            aria-hidden="true"
                          ></i>
                          10/5/2017
                        </div>
                      </div>
                      <div class="blog-des">
                        Lẩu kim chi chua cay lại thêm vị ngọt của hải sản, cực
                        kỳ tuyệt vời luôn nhé!
                      </div>
                      <div class="next-info">
                        <a href="#" class="button"> đọc tiếp &#8811;</a>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
<!-- scrip -->
      <div id="detail-product-modal" class="modal1">
        <div class="modal-item-content">
          <button class="close">&times;</button>
          <div class="row">
            <div class="col-lg-5">
              <div class="detail-product-image">
                <div class="image-detail-product">
                  <img
                    src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_master.jpg"
                    alt=""
                    id="img_01"
                  />
                </div>
                <div class="list-image-products">
                  <ul class="owl-carousel owl-theme" id="list-small">
                    <li class="image-item">
                      <div class="active1">
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-1_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-2_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-2_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-3_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-3_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-4_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-4_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-5_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-5_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-6_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-6_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-7_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-7_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-nau-khoai-tay-bi_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-nau-khoai-tay-bi_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-nuong-sa-te_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-nuong-sa-te_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-chanh-day_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-chanh-day_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-xo_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-xo_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-xao-xa-ot_small.jpg"
                          alt="Cồi sò điệp"
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-xao-xa-ot_master.jpg"
                        />
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-7">
              <div class="detail-product-content">
                <h2><a href="http://">Cồi sò điệp </a></h2>
                <div class="detail-product-info">
                  <p>
                    - Cồi sò điệp là phần thịt được tách từ vỏ của sò điệp. Cồi
                    sò điệp là phần ngon và quý nhất của sò, thịt trắng, dai, có
                    vị giòn ngọt, tính hiền... Cồi sò điệp chứa nhiều dưỡng...
                  </p>

                  <div class="detail-price-product">
                    <span>128,000</span>
                    <div><label> Giá gốc:</label><del>160,000</del></div>
                    <div><label> Giảm:</label> <span>32,000(20%)</span></div>
                  </div>
                </div>
                <div class="detail-product-weight">
                  <label> Khối lượng</label>
                  <div class="select-info">
                    <label class="active"
                      ><input type="radio" name="weight" checked />500g
                      <span class="checkmark"></span
                    ></label>

                    <label
                      ><input type="radio" name="weight" />1kg
                      <span class="checkmark"></span
                    ></label>
                  </div>
                </div>
                <div class="detail-product-number">
                  <div class="number-button">
                    <label>Số lượng</label>
                    <div class="number-pro clearfix">
                      <input type="number" max="100" min="1" value="1" />
                    </div>
                  </div>
                  <button>Thêm vào giỏ</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="cart-box-modal" class="modal1">
        <button class="close">&times;</button>
        <div class="cart-box-modal-content">
          <p>Bạn vừa thêm <a href="http://">Cồi sò điệp</a> vào giỏ hàng</p>
          <div class="modal-button">
            <a href="http://"
              ><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>Tiếp
              tục mua hàng</a
            ><a href="http://"
              ><i class="fa fa-money" aria-hidden="true"></i>Đặt hàng ngay
            </a>
          </div>
        </div>
      </div>
      <script>
          document.getElementById("menu").classList.add("active");
      </script>      
@endsection