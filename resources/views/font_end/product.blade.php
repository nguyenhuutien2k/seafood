@extends('font_end.layouts.master')
@section('title', 'San pham')
@section('css')
<link rel="stylesheet" href="./assets/css/product.css" />
@endsection
@section('content')
<div>
        <div class="breadcrumb-page">
          <div class="container">
            <ul class="breadcrumb-header">
              <li class="breadcrumb-header-item">
                <a href="./index.html">Trang chủ</a>
              </li>
              <li class="breadcrumb-header-item">
                <a href="./product.html">Danh mục</a>
              </li>
              <li class="breadcrumb-header-item active">
                <span>Tất cả sản phẩm </span>
              </li>
            </ul>
          </div>
        </div>
        <div class="mt-4">
          <div class="container">
            <!-- banner -->
            <div class="row">
              <div class="col-xl-12">
                <div class="image-banner">
                  <img
                    src="https://theme.hstatic.net/1000181509/1000452638/14/collection_image_default.jpg?v=44"
                    alt="banner image"
                  />
                </div>
              </div>
            </div>
            <!-- products -->
            <div class="row mt-4">
              <div class="col-lg-3">
                <div class="title-filter" id="button-filter">
                  <span>lọc sản phẩm</span>
                </div>
                <div class="filter-box-list">
                  <div class="filter-box">
                    <p>Loại sản phẩm</p>
                    <ul class="checkbox">
                      <li>
                        <label>
                          Ngao-Sò-Ốc<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          Tôm<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          Khác<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label
                          >Tôm tít<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          Hàu<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                    </ul>
                  </div>
                  <div class="filter-box">
                    <p>Thương hiệu</p>
                    <ul class="checkbox">
                      <li>
                        <label>
                          Khác<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                    </ul>
                  </div>
                  <div class="filter-box">
                    <p>Giá</p>
                    <ul class="checkbox">
                      <li>
                        <label>
                          Tất cả<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          0 Đ ~ 100.000 Đ<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          100.000 Đ ~ 200.000 Đ<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label
                          >200.000 Đ ~ 300.000 Đ <input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          300.000 Đ ~ 500.000 Đ<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label
                          >500.000 Đ ~ 1.000.000<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          Trên 1.000.000 Đ<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                    </ul>
                  </div>
                  <div class="filter-box">
                    <p>Kích thước</p>
                    <ul class="checkbox">
                      <li>
                        <label>
                          0.5kg<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          1kg<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          2kg<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label
                          >3kg <input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                      <li>
                        <label>
                          4kg<input type="checkbox" /><span
                            class="trademark-checkmark"
                          ></span
                        ></label>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="product-list-box">
                  <h1>Tất cả sản phẩm</h1>
                  <div>
                    <div class="row justify-content-between">
                      <div class="view-change col-xs-3">
                        <button class="active" data-list="small-product-list">
                          <span class="fa fa-th"></span>
                        </button>
                        <button data-list="large-product-list">
                          <span class="fa fa-th-list"></span>
                        </button>
                      </div>
                      <div class="col-xs-4">
                        <div class="sort-filter ">
                          <select>
                            <option value="2">Sản phẩm nổi bật</option>
                            <option value="3">Giá: Tăng dần</option>
                            <option value="4">Giá: Giảm dần</option>
                            <option value="5">Tên: A-Z</option>
                            <option value="6">Tên: Z-A</option>
                            <option value="7">Mới nhất</option>
                            <option value="8">Bán chạy nhất</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="product-list ">
                    <ul class="row active" id="small-product-list">
                      <!-- 1 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="./contact.html" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 5 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 6 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 7 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 8 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 1 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 5 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 6 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 7 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!-- 8 -->
                      <li class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                        <div class="product-item">
                          <div class="product-image">
                            <a href="./product_detail.html"
                              ><img
                                src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                alt="Cồi sò điệp"
                            /></a>

                            <div class="label-sale"><span>-20%</span></div>
                            <div class="quick-view">
                              <a href="#" class="cart-item-modal">
                                <i
                                  class="fa fa-shopping-bag"
                                  aria-hidden="true"
                                ></i
                              ></a>
                              <a
                                href="./product_detail.html"
                                class="detail-eye"
                              >
                                <i class="fa fa-eye" aria-hidden="true"></i
                              ></a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3>
                              <a
                                href="./product_detail.html"
                                title="Cồi sò điệp"
                              >
                                Cồi sò điệp</a
                              >
                            </h3>
                            <div class="product-price">
                              <span class="price">128,000</span>
                              <del>160,000</del>
                            </div>
                          </div>
                          <div class="cart-button">
                            <a href="http://">
                              Mua hàng<i
                                class="fa fa-shopping-cart"
                                aria-hidden="true"
                              ></i
                            ></a>
                          </div>
                        </div>
                      </li>
                      <!--  -->
                    </ul>
                    <ul id="large-product-list">
                      <!-- 1 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 5 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 6 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 7 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 8 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 9 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 10 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 11 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 12 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 13 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 14 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 15 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <!-- 16 -->
                      <li class="product-item-large">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="product-image">
                              <a href="./product_detail.html"
                                ><img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt="Cồi sò điệp"
                              /></a>
                              <div class="label-sale"><span>-20%</span></div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="product-info">
                              <h3>
                                <a
                                  href="./product_detail.html"
                                  title="Cồi sò điệp"
                                >
                                  Cồi sò điệp</a
                                >
                              </h3>
                              <div class="product-price">
                                <span class="price">128,000</span>
                                <del>160,000</del>
                              </div>
                              <div class="button-product">
                                <button class="btn">
                                  <i
                                    class="fa fa-shopping-cart"
                                    aria-hidden="true"
                                  ></i
                                  >Thêm vào giỏ
                                </button>
                                <a
                                  href="./product_detail.html"
                                  class="detail-eye btn"
                                >
                                  <i class="fa fa-eye" aria-hidden="true"></i
                                ></a>
                                <a href="http://" class="btn"
                                  ><i class="fa fa-clone" aria-hidden="true"></i
                                ></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!--  -->
          </div>
        </div>
      </div>
<!-- script -->
<div id="detail-product-modal" class="modal1">
        <div class="modal-item-content">
          <button class="close">&times;</button>
          <div class="row">
            <div class="col-lg-5">
              <div class="detail-product-image">
                <div class="image-detail-product">
                  <a href="http://">
                    <img
                      src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_master.jpg"
                      alt=""
                      id="img_01"
                  /></a>
                </div>
                <div class="list-image-products">
                  <ul class="owl-carousel owl-theme" id="list-small">
                    <li class="image-item">
                      <div class="active1">
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-1_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-2_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-2_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-3_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-3_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-4_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-4_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-5_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-5_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-6_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-6_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-7_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-7_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-nau-khoai-tay-bi_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-nau-khoai-tay-bi_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-nuong-sa-te_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-nuong-sa-te_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-chanh-day_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-chanh-day_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-xo_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-xo_master.jpg"
                        />
                      </div>
                    </li>
                    <li class="image-item">
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-xao-xa-ot_small.jpg"
                          alt=""
                          data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-xao-xa-ot_master.jpg"
                        />
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-7">
              <div class="detail-product-content">
                <h2><a href="http://">Cồi sò điệp </a></h2>
                <div class="detail-product-info">
                  <p>
                    - Cồi sò điệp là phần thịt được tách từ vỏ của sò điệp. Cồi
                    sò điệp là phần ngon và quý nhất của sò, thịt trắng, dai, có
                    vị giòn ngọt, tính hiền... Cồi sò điệp chứa nhiều dưỡng...
                  </p>

                  <div class="detail-price-product">
                    <span>128,000</span>
                    <div><label> Giá gốc:</label><del>160,000</del></div>
                    <div><label> Giảm:</label> <span>32,000(20%)</span></div>
                  </div>
                </div>
                <div class="detail-product-weight">
                  <label> Khối lượng</label>
                  <div class="select-info">
                    <label class="active"
                      ><input type="radio" name="weight" checked />500g
                      <span class="checkmark"></span
                    ></label>

                    <label
                      ><input type="radio" name="weight" />1kg
                      <span class="checkmark"></span
                    ></label>
                  </div>
                </div>
                <div class="detail-product-number">
                  <div class="number-button">
                    <label>Số lượng</label>
                    <div class="number-pro clearfix">
                      <input type="number" max="100" min="1" value="1" />
                    </div>
                  </div>
                  <button>Thêm vào giỏ</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="cart-box-modal" class="modal1">
        <button class="close">&times;</button>
        <div class="cart-box-modal-content">
          <p>Bạn vừa thêm <a href="http://">Cồi sò điệp</a> vào giỏ hàng</p>
          <div class="modal-button">
            <a href="http://"
              ><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>Tiếp
              tục mua hàng</a
            ><a href="http://"
              ><i class="fa fa-money" aria-hidden="true"></i>Đặt hàng ngay
            </a>
          </div>
        </div>
      </div>
@endsection