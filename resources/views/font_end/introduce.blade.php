@extends('font_end.layouts.master')
@section('title', 'Giới thiệu về trang web seafood.com')
@section('css')
<link rel="stylesheet" href="./assets/css/contact.css" />
@endsection
@section('content')
<div style="max-width: 100%; width: 100%">
<div>
        <div class="breadcrumb-page">
          <div class="container">
            <ul class="breadcrumb-header">
              <li class="breadcrumb-header-item">
                <a href="#">Trang chủ</a>
              </li>

              <li class="breadcrumb-header-item active">
                <span>Blog - Tin tức </span>
              </li>
            </ul>
          </div>
        </div>
        <div class="container mt-4">
          <div class="row">
            <div class="col-lg-2">
              <div class="list-contact">
                <ul>
                  <li class="active"><a href="http://">Giới thiệu</a></li>
                  <li><a href="http://">Liên hệ</a></li>
                  <li><a href="http://">Thanh toán-giao nhận </a></li>
                  <li><a href="http://">Chính sách đổi trả</a></li>
                  <li><a href="http://">Hướng dẫn mua hàng</a></li>
                  <li><a href="http://"> Chính sách bảo mật</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-10 col-md-12">
              <div class="contact-content">
                <h1>Giới thiệu</h1>
                <p>
                  Seafood chuyên cung cấp sỉ và lẻ các loại hải sản tươi sống,
                  hải sản đóng hộp với các sản phẩm chính sau:
                </p>
                <ul class="product-introduce">
                  <li>Tôm</li>
                  <li>Cá</li>
                  <li>Hàu</li>
                  <li>Bào ngư</li>
                  <li>Ngao-Sò-Ốc</li>
                  <li>Mực</li>
                  <li>Chả cá và các loại hải sản...</li>
                </ul>
                <p>
                  Những sản phẩm cung cấp đến bạn là 100% từ thiên nhiên luôn
                  tươi ngon, sạch. Hầu hết các sản phẩm có nguồn gốc từ các vùng
                  biển Cam Ranh, Phan Thiết, Nha Trang....
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection