@extends('font_end.layouts.master')
@section('css')
<link rel="stylesheet" href="./assets/css/contact.css" />
@endsection
@section('content')
<div style="max-width: 100%; width: 100%">
<div>
        <div class="breadcrumb-page">
          <div class="container">
            <ul class="breadcrumb-header">
              <li class="breadcrumb-header-item">
                <a href="#">Trang chủ</a>
              </li>

              <li class="breadcrumb-header-item active">
                <span>Blog - Tin tức </span>
              </li>
            </ul>
          </div>
        </div>
        <div class="container mt-4">
          <div class="row">
            <div class="col-lg-2">
              <div class="list-contact">
                <ul>
                  <li><a href="http://">Giới thiệu</a></li>
                  <li class="active"><a href="http://">Liên hệ</a></li>
                  <li><a href="http://">Thanh toán-giao nhận </a></li>
                  <li><a href="http://">Chính sách đổi trả</a></li>
                  <li><a href="http://">Hướng dẫn mua hàng</a></li>
                  <li><a href="http://"> Chính sách bảo mật</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="contact-content">
                <h1>Liên hệ</h1>
                <div class="map-contact">
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d772.095822376552!2d106.65592234946958!3d10.787128143695865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ecbb734d163%3A0xba08fd5c66f39411!2zSOG6u20gNTYgVsOibiBDw7RpLCBwaMaw4budbmcgNywgVMOibiBCw6xuaCwgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1600142885988!5m2!1svi!2s"
                    frameborder="0"
                    style="border: 0; width: 100%; height: 455px"
                    allowfullscreen=""
                    aria-hidden="false"
                    tabindex="0"
                  ></iframe>
                </div>
                <div>
                  <div class="row">
                    <div class="col-lg-6 contact-child">
                      <h3>Liên hệ với chúng tôi</h3>
                      <div class="contact-form">
                        <p>
                          Nếu bạn có thắc mắc gì, có thể gửi yêu cầu cho chúng
                          tôi, và chúng tôi sẽ liên lạc với bạn sớm nhất có thể.
                        </p>
                        <div>
                          <input type="text" placeholder="Tên của bạn" />
                          <input
                            type="text"
                            placeholder="Số điên thoại của bạn"
                          />
                          <input type="text" placeholder="Email của bạn" />
                          <textarea placeholder="Viết bình luận "></textarea>
                          <button>Gửi liên hệ</button>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6 contact-child">
                      <h3>Địa chỉ liên lạc</h3>
                      <div class="info-company">
                        <h3>Công ty cổ phần Haravan</h3>
                        <p>
                          Giải pháp ban hàng điện tử từ website đến cửa hàng
                        </p>
                        <ul class="info-address">
                          <li>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <span>56 Van Coi, Q. Tân Bình, TP.Hồ Chí Minh</span>
                          </li>
                          <li>
                            <i class="fa fa-envelope" aria-hidden="true"></i
                            ><span>h1@haravan.com</span>
                          </li>
                          <li>
                            <i class="fa fa-phone" aria-hidden="true"></i
                            ><span>1900.636.099</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection