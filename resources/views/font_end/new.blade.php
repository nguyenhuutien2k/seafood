@extends('font_end.layouts.master')
@section('title', 'Tin tuc')
@section('css')
<link rel="stylesheet" href="./assets/css/news.css" />
@endsection
@section('content')
<div>
        <div class="breadcrumb-page">
          <div class="container">
            <ul class="breadcrumb-header">
              <li class="breadcrumb-header-item">
                <a href="#">Trang chủ</a>
              </li>

              <li class="breadcrumb-header-item active">
                <span>Blog - Tin tức </span>
              </li>
            </ul>
          </div>
        </div>
        <div>
          <div class="container mt-4">
            <div class="row">
              <div class="col-lg-9 col-md-8">
                <h1 class="mb-4">Tin tức</h1>
                <div class="news-list">
                  <div class="news-item">
                    <div class="news-image">
                      <img
                        src="https://file.hstatic.net/1000181509/article/ban-da-biet-bia-thu-cong-la-bia-gi-hay-chua_large.jpg"
                        alt="Bạn đã biết bia thủ công là bia gì chưa?"
                      />
                    </div>
                    <div class="news-detail">
                      <h2>
                        <a href="http://"
                          >Bạn đã biết bia thủ công là bia gì chưa?</a
                        >
                      </h2>
                      <p>Ngày: 08/05/2017 lúc 17:37PM</p>
                      <strong class="description">
                        Dù rất phổ biến ở các nước phương Tây phát triển như Mỹ,
                        Australia, các nước Châu Âu..., nhưng "bia thủ công" vẫn
                        là khái niệm khá xa lạ đối với người Việt Nam. Bia thủ
                        công thường có giá thành cao hơn so với bia thường và
                        được cho là chất lượng đáng kể hơn. Vậy bia thủ công là
                        gì?
                      </strong>
                    </div>
                  </div>
                  <div class="news-item">
                    <div class="news-image">
                      <img
                        src="https://file.hstatic.net/1000181509/article/ban-da-biet-bia-thu-cong-la-bia-gi-hay-chua_large.jpg"
                        alt="Bạn đã biết bia thủ công là bia gì chưa?"
                      />
                    </div>
                    <div class="news-detail">
                      <h2>
                        <a href="http://"
                          >Bạn đã biết bia thủ công là bia gì chưa?</a
                        >
                      </h2>
                      <p>Ngày: 08/05/2017 lúc 17:37PM</p>
                      <strong class="description">
                        Dù rất phổ biến ở các nước phương Tây phát triển như Mỹ,
                        Australia, các nước Châu Âu..., nhưng "bia thủ công" vẫn
                        là khái niệm khá xa lạ đối với người Việt Nam. Bia thủ
                        công thường có giá thành cao hơn so với bia thường và
                        được cho là chất lượng đáng kể hơn. Vậy bia thủ công là
                        gì?
                      </strong>
                    </div>
                  </div>
                  <div class="news-item">
                    <div class="news-image">
                      <img
                        src="https://file.hstatic.net/1000181509/article/ban-da-biet-bia-thu-cong-la-bia-gi-hay-chua_large.jpg"
                        alt="Bạn đã biết bia thủ công là bia gì chưa?"
                      />
                    </div>
                    <div class="news-detail">
                      <h2>
                        <a href="http://"
                          >Bạn đã biết bia thủ công là bia gì chưa?</a
                        >
                      </h2>
                      <p>Ngày: 08/05/2017 lúc 17:37PM</p>
                      <strong class="description">
                        Dù rất phổ biến ở các nước phương Tây phát triển như Mỹ,
                        Australia, các nước Châu Âu..., nhưng "bia thủ công" vẫn
                        là khái niệm khá xa lạ đối với người Việt Nam. Bia thủ
                        công thường có giá thành cao hơn so với bia thường và
                        được cho là chất lượng đáng kể hơn. Vậy bia thủ công là
                        gì?
                      </strong>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-4">
                <div class="blog-banner">
                  <h3>bài viết mới nhất</h3>
                  <div class="blog-content">
                    <ul>
                      <!-- 1 -->
                      <li class="blog-item">
                        <div class="blog-image">
                          <a href="http://"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/cach-lam-lau-kim-chi-hai-san_eca4b6aa865748c2890ae5ca90f7649b_medium.jpg"
                              alt="Cách làm nẩu kim chi hải sản"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h4>
                            <a href="http://">Cách làm nẩu kim chi hải sản</a>
                          </h4>
                          <div class="post-infomation">
                            10/502017<br />
                            Đăng bởi : Quách Hương
                          </div>
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="blog-item">
                        <div class="blog-image">
                          <a href="http://"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/gio-den-luot-hau-ngao-so-cung-mac-phai-ung-thu-nhu-nguoi_medium.jpg"
                              alt="Giờ đến lượt hàu, ngao, sò,... cũng mắc phải ung thư như người"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h4>
                            <a href="http://"
                              >Giờ đến lượt hàu, ngao, sò,... cũng mắc phải ung
                              thư như người</a
                            >
                          </h4>
                          <div class="post-infomation">
                            10/502017<br />
                            Đăng bởi : Quách Hương
                          </div>
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="blog-item">
                        <div class="blog-image">
                          <a href="http://"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/muc-chien-xu-sot-wasabi-an-mot-mieng-la-me-man-ngay_011f174901b547af81ac7162084b00d7_medium.jpg"
                              alt="Mực chiên xù sốt wasabi ăn một miếng và mê mẩn ngay"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h4>
                            <a href="http://"
                              >Mực chiên xù sốt wasabi ăn một miếng và mê mẩn
                              ngay</a
                            >
                          </h4>
                          <div class="post-infomation">
                            10/502017<br />
                            Đăng bởi : Quách Hương
                          </div>
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="blog-item">
                        <div class="blog-image">
                          <a href="http://"
                            ><img
                              src="https://file.hstatic.net/1000181509/article/nhung-loi-ich-tuyet-voi-tu-ca_medium.jpg"
                              alt="Những lợi ích tuyệt vời từ cá"
                          /></a>
                        </div>
                        <div class="blog-item-child">
                          <h4>
                            <a href="http://">Những lợi ích tuyệt vời từ cá</a>
                          </h4>
                          <div class="post-infomation">
                            10/502017<br />
                            Đăng bởi : Quách Hương
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="blog-banner">
                  <h3>danh mục sản phẩm</h3>
                  <div class="category-content">
                    <ul>
                      <li class="category-item">
                        <a href="http://">Trang chủ</a>
                      </li>
                      <li class="category-item">
                        <a href="http://"> Sản phẩm</a>
                      </li>
                      <li class="category-item">
                        <a href="http://">Tin tức</a>
                        <span id="drop-menu"></span>
                        <ul>
                          <li><a href="http://"> Tin hải sản </a></li>
                          <li><a href="http://">Món ngon mỗi ngày</a></li>
                        </ul>
                      </li>
                      <li class="category-item">
                        <a href="http://">Giới thiệu </a>
                      </li>
                      <li class="category-item">
                        <a href="http://">Liên hệ</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection