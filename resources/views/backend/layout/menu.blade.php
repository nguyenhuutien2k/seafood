<aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img class="user-avatar rounded-circle" src="http://theme.hstatic.net/1000181509/1000452638/14/logo.png?v=44" alt=""></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-dashboard"></i>Thông báo </a>
                    </li>
                    <li class="">
                        <a href="{{route('danhmuc')}}"> <i class="menu-icon fa fa-table"></i>Danh mục sản phẩm</a>
                    </li>
                    <li class="">
                        <a href="{{route('sanpham')}}"> <i class="menu-icon fa fa-inbox"></i>Sản phẩm</a>
                    </li>
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-camera-retro"></i>Slide</a>
                    </li>
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-table"></i>Danh mục tin tức</a>
                    </li>
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-file"></i>Tin tức</a>
                    </li>
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-cutlery"></i>Đầu bếp</a>
                    </li>
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-users"></i>Tài khoản</a>
                    </li>
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-comment"></i>Bình luận</a>
                    </li>
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-phone"></i>Liên hệ - Phản hồi</a>
                    </li>
                    <li class="">
                        <a href=""> <i class="menu-icon fa fa-gear"></i>Thương hiệu</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>